package koochaiyaphum.nitisak.lab9;
/*
 * Author: Nitisak Koochaiyaphum
 *Id: 613040502-9
 *Sec: 1
 *Date: April, 10,2019
 * 
 */
import javax.swing.JFrame;
import javax.swing.SwingUtilities;


public class MyFrameV6 extends MyFrameV5  {
	public MyFrameV6(String text) {
		super(text);
	}

	public static void main (String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run () {
				createAndShowGUI();
			}
		});
	}
	
	public static void createAndShowGUI() {
		MyFrameV6 msw = new MyFrameV6 ("My Frame V6");
		msw.addComponents();
		msw.setFrameFeatures ();
	}
	
	protected void addComponents() {
		add(new MyCanvasV6());
			
	}
	protected void setFrameFeatures() {
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
