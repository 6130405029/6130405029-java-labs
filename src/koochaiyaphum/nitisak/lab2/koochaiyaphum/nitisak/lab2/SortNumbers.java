/**This Java program that accepts three arguments:you will input 
* 5 number of digits. 
* 
* Author: Nitisak Koochaiyaphum
* ID: 61304050-9
* Sec: 1
* Date: January 26, 2019
**/
package koochaiyaphum.nitisak.lab2;
import java.util.Arrays; 
public class SortNumbers {
	public static void main(String[] args) {
		double[] arr = {2.3,11.5,7,9.1,23};
		Arrays.sort(arr);
		for (int i=0; i<arr.length; i++) 
        System.out.println(arr[i]);
	}
}
