package koochaiyaphum.nitisak.lab9;
/*
 * Author: Nitisak Koochaiyaphum
 *Id: 613040502-9
 *Sec: 1
 *Date: April, 10,2019
 * 
 */
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class MyFrameV7 extends MyFrameV6 {
	public MyFrameV7(String text) {
		super(text);
	}

	public static void main (String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run () {
				createAndShowGUI();
			}
		});
	}
	
	public static void createAndShowGUI() {
		MyFrameV7 msw = new MyFrameV7 ("My Frame V7");
		msw.addComponents();
		msw.setFrameFeatures ();
	}
	
	protected void addComponents() {
		add(new MyCanvasV7());
			
	}
	protected void setFrameFeatures() {
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
