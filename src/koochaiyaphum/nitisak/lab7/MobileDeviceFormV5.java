package koochaiyaphum.nitisak.lab7;
/*
 * Author: Nitisak Koochaiyaphum
 *Id: 613040502-9
 *Sec: 1
 *Date: April, 18,2019
 * 
 */
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.Image;
import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

public class MobileDeviceFormV5 extends MobileDeviceFormV4 {

	protected JPanel panelImagePhone;
	protected ImageIcon imagePhone;

	protected MobileDeviceFormV5(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	protected void addComponents() {

		initComponents();

	}

	protected void initComponents() {
		/*
		 * 
		 * this method will set sizr and type of fonts.
		 * 
		 * @return nothing
		 * 
		 */
		super.addComponents();
		Font serifFont = new Font("Serif", Font.PLAIN, 14);
		Font serifBoldFont = new Font("Serif", Font.BOLD, 14);
		brandname.setFont(serifFont);
		modelName.setFont(serifFont);
		weight.setFont(serifFont);
		price.setFont(serifFont);
		system.setFont(serifFont);
		typeLabel.setFont(serifFont);
		reviewLabel.setFont(serifFont);
		featuresList.setFont(serifFont);
		typeCombo.setFont(serifFont);
		buttonCancel.setFont(serifFont);
		buttonOK.setFont(serifFont);
		iOS.setFont(serifFont);
		android.setFont(serifFont);
		brandText.setFont(serifBoldFont);
		modelText.setFont(serifBoldFont);
		weightText.setFont(serifBoldFont);
		priceText.setFont(serifBoldFont);
		reviewArea.setFont(serifBoldFont);
		buttonCancel.setForeground(Color.RED);
		buttonOK.setForeground(Color.BLUE);

	}

	public static void createAndShowGUI() {
		MobileDeviceFormV5 mobileDeviceFormV5 = new MobileDeviceFormV5("MobileDeviceFormV5");
		mobileDeviceFormV5.addComponents();
		mobileDeviceFormV5.addMenus();
		mobileDeviceFormV5.setFrameFeatures();

	}

	protected void setFrameFeatures() {
		this.setLocationRelativeTo(null);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		pack();
		setVisible(true);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}

		});
	}
}
