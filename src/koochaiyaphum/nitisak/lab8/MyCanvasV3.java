package koochaiyaphum.nitisak.lab8;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class MyCanvasV3 extends MyCanvasV2 {

	private static final long serialVersionUID = 1L;

	public void paint(Graphics g) {
		super.paint(g);
		
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.black);
		g2d.fillRect(0, 0, this.getWidth(), this.getHeight());

		MyBall[] ball = new MyBall[4];
		MyBall ball1 = new MyBall(0, 0);
		MyBall ball2 = new MyBall(this.getWidth() - MyBall.diameter, 0);
		MyBall ball3 = new MyBall(0, this.getHeight() - MyBall.diameter);
		MyBall ball4 = new MyBall(this.getWidth() - MyBall.diameter, this.getHeight() - MyBall.diameter);

		ball[0] = ball1;
		ball[1] = ball2;
		ball[2] = ball3;
		ball[3] = ball4;
		
		

		g2d.setColor(Color.WHITE);
		for (int i = 0; i < ball.length; i++) {
			g2d.fill(ball[i]);
		}

		int width1 = 80;
		brick = new MyBrick(0, (this.getHeight() / 2) - 20);
		for (int i = 0; i < 17; i++) {
			brick.x = i * width1;
			g2d.setColor(Color.WHITE);
			g2d.fill(brick);
			g2d.setStroke(new BasicStroke(5));
			g2d.setColor(Color.BLACK);
			g2d.draw(brick);
		}

	}
}
