package koochaiyaphum.nitisak.lab8;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;

import javax.swing.JPanel;

public class MyCanvas extends JPanel {
	
	public final static int WIDTH = 800;
	public final static int HEIGHT = 600;
	public MyCanvas() {
		super();
		setPreferredSize(new Dimension(WIDTH,HEIGHT));
		setBackground(Color.BLACK);	
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D)g;
		
		g2d.setColor(Color.WHITE);
		g2d.drawOval((WIDTH-300) / 2, (HEIGHT-300)/2, 300, 300); 
		g2d.fillOval((WIDTH-150) / 2, (HEIGHT-150)/2, 30, 60);
		g2d.fillOval((WIDTH+90) / 2, (HEIGHT-150)/2, 30, 60);
		g2d.fillOval((WIDTH-150) / 2, (HEIGHT-150)/2, 30, 60);
		
		g2d.fill3DRect((WIDTH-100) / 2, (HEIGHT+100)/2, 100, 10, true);
	}

}
