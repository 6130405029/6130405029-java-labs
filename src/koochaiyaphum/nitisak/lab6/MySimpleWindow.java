package koochaiyaphum.nitisak.lab6;
/*
 * Author:Nitisak Koochaiyaphum
 * Id: 613040502-9	
 * Sec: 1
 * Date: March 29,2019
 * 
 * 
 * 
 * 
 */
import javax.swing.JFrame;
import java.awt.BorderLayout;
	import java.awt.event.ActionEvent;
	import java.awt.event.ActionListener;
	import javax.swing.JButton;
	import javax.swing.JComboBox;
	import javax.swing.JFrame;
	import javax.swing.JLabel;
	import javax.swing.JList;
	import javax.swing.JPanel;
import javax.swing.SwingUtilities;
public class MySimpleWindow extends JFrame {
	
	protected static final long serialVersionUID = -8895159383486813219L;
	protected JPanel panel;
	protected JButton cancelButton;
	protected JButton okButton;
	protected JPanel buttonPanel;
	protected JPanel windowPanel;
	public MySimpleWindow(String title) {
		super(title);
	}

	protected void addComponents() {
		buttonPanel = new JPanel();
		cancelButton = new JButton("Cancel");
		okButton = new JButton("OK");
		buttonPanel.add(cancelButton);
		buttonPanel.add(okButton);
		windowPanel = (JPanel) this.getContentPane();
		windowPanel.setLayout(new BorderLayout());
		windowPanel.add(buttonPanel, BorderLayout.SOUTH);

	}

	public static void createAndShowGUI() {
		MySimpleWindow window = new MySimpleWindow("My Simple Window");
		window.addComponents();
		window.setFrameFeature();
	}

	protected void setFrameFeature() {
		this.setLocationRelativeTo(null);
		pack();
		this.setSize(300, 100);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
