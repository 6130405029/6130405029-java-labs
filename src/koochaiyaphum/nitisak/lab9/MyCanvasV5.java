package koochaiyaphum.nitisak.lab9;
/*
 * Author: Nitisak Koochaiyaphum
 *Id: 613040502-9
 *Sec: 1
 *Date: April, 10,2019
 * 
 */
import koochaiyaphum.nitisak.lab8.MyBall;
import koochaiyaphum.nitisak.lab8.MyCanvasV4;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Rectangle2D.Double;


import koochaiyaphum.nitisak.lab8.MyBall;
import koochaiyaphum.nitisak.lab8.MyCanvas;
import koochaiyaphum.nitisak.lab8.MyCanvasV4;

public class MyCanvasV5 extends MyCanvasV4 implements Runnable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected MyBallV2 ball = new MyBallV2(0, HEIGHT / 2 - MyBall.diameter / 2);
	protected Thread running = new Thread(this);

	public MyCanvasV5() {
		ball.ballVelX = 2;
		ball.ballVelY = 0;
		running.start();

	}

	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.BLACK);
		g2d.fill(new Rectangle2D.Double(0, 0, this.getWidth(), this.getHeight()));
		g2d.setColor(Color.WHITE);
		g2d.fill(ball);

	}

	public void run() {
		System.out.println(super.WIDTH);
		while (true) {
			
			System.out.println(ball.x);
			if ((int)ball.x + MyBall.diameter >= super.WIDTH - 30) {
				
				ball.ballVelX = 0;
				break;
			}
			ball.move();
			repaint();
			try {
				Thread.sleep(10);
			} catch (InterruptedException ex) {

			}
		}
	}
}
