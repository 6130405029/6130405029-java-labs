/**This Java program that accepts three arguments: A program will output 
* my information
* my name
* my ID
* my first latter + "True" + theLastTwoDigitsOfYourlIDNumber
* 
*Author: Nitisak Koochaiyaphum
* ID: 61304050-9
* Sec: 1
* Date: January 21, 2019
*
**/
package koochaiyaphum.nitisak.lab2;
public class DataTypes {
	public static void main(String[] args) {
			String Name = "nitisak koochaiyaphum";
			String ID ="6130405029";
			char first = Name.charAt(0);
			boolean real = true;
			int myIDInOctal = 035;
			int  hex = 0x1D;
			int oct = 35;
			int lastID = 29;
			float FirstID = 29.61f;
			double theLastTwoDigitsOfYourIDNumber = 29.61d;
			
			
			System.out.println("My name is " + Name);
			System.out.println("My student ID is " + ID);
			System.out.println(first + " " + real + " " + myIDInOctal + " " + hex + " " + oct);
			System.out.println(lastID + " " + FirstID + " " + theLastTwoDigitsOfYourIDNumber);
		}
}
	
