package koochaiyaphum.nitisak.lab7;
import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

import koochaiyaphum.nitisak.lab6.MobileDeviceFormV3;

/*
 * Author:Nitisak Koochaiyaphum
 * Id: 613040502-9	
 * Sec: 1
 * Date: March 29,2019
 */
;

public class MobileDeviceFormV4 extends MobileDeviceFormV3{
	
protected JMenuItem size16, size20, size24 ,red ,green ,blue;
	

	public MobileDeviceFormV4(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}
	public static void createAndShowGUI() {
		MobileDeviceFormV4 msw = new MobileDeviceFormV4("Mobile Device Form V4");
		msw.addComponents();
		msw.addMenus();
		msw.setFrameFeatures();
	}
	protected void addMenus() {
     super.addMenus();
     updateMenusIcon();
     addSupMenus();
	}
	protected void addSupMenus() {
		 menuConfig.remove(colorItem);
		 menuConfig.remove(sizeItem);
	    colorItem = new JMenu("color");
		sizeItem = new JMenu("size");
		red = new JMenuItem("Red");
		green = new JMenuItem("Green");
		blue = new JMenuItem("Blue");
		size16 = new JMenuItem("size16");
		size20 = new JMenuItem("size20");
		size24 = new JMenuItem("size24");
		sizeItem.add(size16);
		sizeItem.add(size20);
		sizeItem.add(size24);
		
		colorItem.add(red);
		colorItem.add(green);
		colorItem.add(blue);
		menuConfig.add(colorItem);
		menuConfig.add(sizeItem);
		
		
	}
	
	
	
	protected void updateMenusIcon() {
		newItem.setIcon(new ImageIcon(getClass().getResource("images/new.jpg") ) );
		
		
	}

	
	protected void addComponents() {
		super.addComponents();
		
	}
	protected void setFrameFeatures() {
		super.setFrameFeature();
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
