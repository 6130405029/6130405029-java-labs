package koochaiyaphum.nitisak.lab9;
/*
 * Author: Nitisak Koochaiyaphum
 *Id: 613040502-9
 *Sec: 1
 *Date: April, 10,2019
 * 
 */


import koochaiyaphum.nitisak.lab8.MyBall;

public class MyBallV2 extends MyBall{
	
		protected int ballVelX;
		protected int ballVelY;

		
		public MyBallV2(int x, int y) {
			super(x, y);
		}

		public void move() {

			super.x += ballVelX;
			super.y += ballVelY;
		}

}
