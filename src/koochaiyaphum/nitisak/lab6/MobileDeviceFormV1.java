package koochaiyaphum.nitisak.lab6;
/*
 * Author:Nitisak Koochaiyaphum
 * Id: 613040502-9	
 * Sec: 1
 * Date: March 29,2019
 * 
 * 
 * 
 * 
 */
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.*;
public class MobileDeviceFormV1 extends MySimpleWindow {
	 protected static final long serialVersionUID = 7644184030727241307L;
		protected JLabel brandName;
		protected JLabel modelName;
		protected JLabel weight;
		protected JLabel price;
		protected JLabel mobileOs;
		protected JRadioButton androidButton;
		protected JRadioButton iosButton;
		protected JTextField brandNameTxtField;
		protected JTextField modelNameTxtField;
		protected JTextField weightTxtField;
		protected JTextField priceTxtField;
		protected JPanel panelForm;
		protected JPanel osPanel;

		public MobileDeviceFormV1(String title) {
			super(title);
		}

		protected void addComponents() {
			super.addComponents();

			brandName = new JLabel("Brand name : ");
			brandNameTxtField = new JTextField(15);

			modelName = new JLabel("Model name : ");
			modelNameTxtField = new JTextField(15);

			weight = new JLabel("Weight (kg.) : ");
			weightTxtField = new JTextField(15);

			price = new JLabel("Price (Baht) : ");
			priceTxtField = new JTextField(15);

			mobileOs = new JLabel("Mobile OS : ");

			ButtonGroup group = new ButtonGroup();
			JRadioButton androidButton = new JRadioButton("Android");
			JRadioButton iosButton = new JRadioButton("iOS");
			group.add(iosButton);
			group.add(androidButton);
			osPanel = new JPanel();
			osPanel.add(androidButton);
			osPanel.add(iosButton);

			
			panelForm = new JPanel(new GridBagLayout());
			GridBagConstraints gridBag = new GridBagConstraints();
			gridBag.insets = new Insets(3, 80, 3, 80);
			gridBag.gridx = 0;
			gridBag.gridy = 0;
			gridBag.anchor = GridBagConstraints.LINE_START;
			panelForm.add(brandName, gridBag);
			gridBag.gridy ++;
			panelForm.add(modelName, gridBag);
			gridBag.gridy ++;
			panelForm.add(weight, gridBag);
			gridBag.gridy ++;
			panelForm.add(price, gridBag);
			gridBag.gridy ++;
			panelForm.add(mobileOs, gridBag);

			
			gridBag.gridx = 1;
			gridBag.gridy = 0;
			gridBag.anchor = GridBagConstraints.LINE_END;
			panelForm.add(brandNameTxtField, gridBag);
			gridBag.gridy ++;
			panelForm.add(modelNameTxtField, gridBag);
			gridBag.gridy ++;
			panelForm.add(weightTxtField, gridBag);
			gridBag.gridy ++;
			panelForm.add(priceTxtField, gridBag);
			gridBag.gridy ++;
			panelForm.add(osPanel, gridBag);
			windowPanel.add(panelForm);
			
		}
		public static void createAndShowGUI() {
			MobileDeviceFormV1 formV1 = new MobileDeviceFormV1("Mobile Device Form V1");
			formV1.addComponents();
			formV1.setFrameFeature();
		}

		protected void setFrameFeature() {
			this.setLocationRelativeTo(null);
			pack();
			setVisible(true);
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}

		public static void main(String[] args) {
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					createAndShowGUI();
				}
			});
		}
}
