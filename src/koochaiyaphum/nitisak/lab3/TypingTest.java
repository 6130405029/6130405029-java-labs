/*A program that test whether a user types faster or slower than an average person. 
* An average person types at the speed of 40 words per minute. 
* The program random 8 colors from a list of rainbow colors 
* (RED, ORANGE, YELLOW, GREEN, BLUE, INDIGO, VIOLET). 
* Color can be picked more than once. A user must type in the same list ignoring case.
*  If the user type in incorrectly, he needs to type in again. 
*  After the correct input is enter, the time used for typing in will be calculated and 
*  if he types in faster than or equal to 12 seconds, he types faster than average otherwise 
*
*Author: Nitisak Koochaiyaphum
* ID: 61304050-9
* Sec: 1
* Date: February 5, 2019
*/
package koochaiyaphum.nitisak.lab3;

import java.util.Scanner;
public class TypingTest {
   	private static final Scanner scanner = new Scanner( System.in );
	static String randomColor() {
	
		String[] color = { "GREEN", "RED", "BLUE", "INDIGO", "VIOLET", "YELLOW", "ORANGE" };
		String colorAll = " ";
		for (int i = 0; i < 8; i++) {
			int rand = (int) (Math.random() * 7);
			if (i == 7) {
				colorAll += color[rand];	
			}
			else {
				colorAll += color[rand] + " ";
			}
			return colorAll;
		}
		
	double timeBefore = System.currentTimeMillis();
	double timeAfter;
	
	while (true) {
		System.out.println(colorAll);
		System.out.print("Type your answer: ");
		Scanner scan = new Scanner(System.in);
		String abc = scan.next().toUpperCase();

		
		if (abc.equals(colorAll)) {
			timeAfter = System.currentTimeMillis();
			System.out.println("Your time is " + ((timeAfter - timeBefore)/1000) + ".");
			System.exit(0);
			}
		
		}
	}
		
}
