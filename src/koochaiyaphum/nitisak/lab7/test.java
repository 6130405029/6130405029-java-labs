package koochaiyaphum.nitisak.lab7;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class test {
   private static class HelloWorldDisplay extends JPanel {
	   /**
	 * 
	 */
	private static final long serialVersionUID = -8624027742570784022L;

	public void paintComponent(Graphics g) {
           super.paintComponent(g);
           g.drawString("Hello World!", 30, 20);
    }
}
private static class ButtonHandler implements ActionListener {
    public void actionPerformed(ActionEvent e) {
           System.exit(0);
    }
}
public static void main(String[] args) {
	   HelloWorldDisplay displayPanel = new HelloWorldDisplay();
	   JButton okButton = new JButton("OK");
	   ButtonHandler listener = new ButtonHandler();
	   okButton.addActionListener(listener);
	   JPanel content = new JPanel();
	   content.setLayout(new BorderLayout());
	   content.add(displayPanel, BorderLayout.CENTER);
	   content.add(okButton, BorderLayout.SOUTH);
	   JFrame window = new JFrame("GUI Test");
	    window.setContentPane(content);
	    window.setSize(250,100);
	    window.setLocation(100,100);
	    window.setVisible(true);
	  }
	}
