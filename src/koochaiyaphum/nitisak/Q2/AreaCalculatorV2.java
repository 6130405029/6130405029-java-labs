package koochaiyaphum.nitisak.Q2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingUtilities;

/*
 * Author: Nitisak Koochaiyaphum
 *Id: 613040502-9
 *Sec: 1
 *Date: April, 29,2019
 *Quiz2
 * This is a program calculator area 2 function Triangle and Rectangle
 * The class AreaCalculatorV2 can calculate area in Triangle and Rectangle.
 */


public class AreaCalculatorV2 extends AreaCalculatorV1 implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public  AreaCalculatorV2(String title) {
		super(title);
	}
	public void createAndShowGUI() {
		createComponents();
		setFrameFeatures();
		addListeners();
		addMAKeys();
	}

	protected void addComponents() {
		super.createComponents();
	}
	
	protected void setFrameFeatures() {
		super.setFrameFeature();
	}
	
	protected void addListeners() {
		resetButton.addActionListener(this);
		
		
		calculatorButton.addActionListener(this);
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
	protected void addMAKeys() {
		
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				AreaCalculatorV2 rac = new AreaCalculatorV2("Area Calculator V2");
				rac.createAndShowGUI();
			}
		});
	}

	
}
