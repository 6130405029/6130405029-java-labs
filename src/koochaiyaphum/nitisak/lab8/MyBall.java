package koochaiyaphum.nitisak.lab8;

import java.awt.geom.Rectangle2D;

public class MyBall extends Rectangle2D.Double{
	public final static int  diameter = 30;
	public MyBall(int x,int y) {
		super(x, y ,diameter, diameter);
	}
}
