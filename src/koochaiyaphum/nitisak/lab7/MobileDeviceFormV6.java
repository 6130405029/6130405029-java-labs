package koochaiyaphum.nitisak.lab7;
/*
 * Author: Nitisak Koochaiyaphum
 *Id: 613040502-9
 *Sec: 1
 *Date: April, 18,2019
 * 
 */
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Window;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

public class MobileDeviceFormV6 extends MobileDeviceFormV5 {

	protected MobileDeviceFormV6(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	protected JPanel imgPanel;
	protected ReadImage content;

	class ReadImage extends JPanel {
		/*
		 * 
		 * this method will read images from my computer;
		 * 
		 * @return nothing
		 * 
		 */
		BufferedImage img;
		String fileName = "image/galaxyNote9.jpg";

		public void paintComponent(Graphics g) {
			g.drawImage(img, 10, 0, null);

		}

		public ReadImage() {
			try {
				img = ImageIO.read(new File(fileName));
			} catch (IOException e) {
				e.printStackTrace(System.err);
			}
		}

		public Dimension getPreferredSize() {
			if (img == null)
				return new Dimension(100, 100);
			else
				return new Dimension(img.getWidth() + 20, img.getHeight() + 60);
		}
	}

	protected void addComponents() {
		/*
		 * 
		 * this method will add images to panel.
		 * 
		 * @return nothing
		 * 
		 */
		super.addComponents();
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(5, 5, 5, 0);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridx = 0;
		
		gbc.gridwidth = 2;
		gbc.gridy = 9;
		
		imgPanel = new JPanel();
		content = new ReadImage();
		imgPanel.add(content);
		buttonPanel.add(imgPanel, gbc);
		allPanel.add(buttonPanel, BorderLayout.WEST);
		windowPanel.add(allPanel, BorderLayout.NORTH);

	}

	public static void createAndShowGUI() {
		MobileDeviceFormV6 mobileDeviceV6 = new MobileDeviceFormV6("MobileDeviceV6");
		mobileDeviceV6.addComponents();
		mobileDeviceV6.addMenus();
		mobileDeviceV6.setFrameFeatures();
	}

	protected void setFrameFeatures() {
		this.setLocationRelativeTo(null);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		pack();
		setVisible(true);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}

		});
	}
}
