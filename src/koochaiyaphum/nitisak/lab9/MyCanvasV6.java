package koochaiyaphum.nitisak.lab9;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import koochaiyaphum.nitisak.lab8.MyBall;
import koochaiyaphum.nitisak.lab8.MyCanvas;
/*
 * Author: Nitisak Koochaiyaphum
 *Id: 613040502-9
 *Sec: 1
 *Date: April, 10,2019
 * 
 */

public class MyCanvasV6 extends MyCanvasV5 implements Runnable{
	Thread running = new Thread(this);
	protected MyBallV2 ball2;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.black);
		g2d.fill(new Rectangle2D.Double(0,0,super.WIDTH, super.HEIGHT));
		g2d.setColor(Color.white);
		g2d.fill(ball2);
	}
	
	public MyCanvasV6() {
		 ball2 = new MyBallV2(MyCanvas.WIDTH/2 - MyBall.diameter/2, MyCanvas.HEIGHT/2 - MyBall.diameter/2);
		 ball2.ballVelX = 1;
		 ball2.ballVelY = 1;
		 running.start();
	}
	
	//ball.x <= 0 || ball.x + MyBall.diameter >= super.WIDTH
	public void run() {
		while (true) {
			if((int)ball2.x <= 0 || ball2.x + MyBall.diameter >= super.WIDTH - 30) {
				ball2.ballVelX *= -1;
				
			}
			if ((int)ball2.y <= 0 || ball2.y + MyBall.diameter >= super.HEIGHT - 30) {
				ball2.ballVelY *= -1;
				
			}
			//System.out.println(ball.x);
			ball2.move();
			repaint();
			try {
				Thread.sleep(10);
			} catch (InterruptedException ex) {

			}
		}
	}

}
