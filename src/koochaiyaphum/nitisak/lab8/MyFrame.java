package koochaiyaphum.nitisak.lab8;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;


public class MyFrame extends JFrame{

	public MyFrame(String text) {
		super(text);

	}

	public static void main(String[] args) { 
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	
	public static void createAndShowGUI() {
		MyFrame msw = new MyFrame("My Frame");
		msw.addComponents();
		msw.setFrameFeatures(); 
	}
	
	protected void addComponents() {
		add(new MyCanvas());
	}
	
	protected void setFrameFeatures() {
		pack();
		setLocationRelativeTo(null); 
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
