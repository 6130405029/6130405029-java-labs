package koochaiyaphum.nitisak.Q2;
/*
 * Author: Nitisak Koochaiyaphum
 *Id: 613040502-9
 *Sec: 1
 *Date: April, 29,2019
 *Quiz2
 * This is a program calculator area 2 function Triangle and Rectangle.
 * The class AreaCalculatorV1 shows GUI interface simple program.
 */
import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;


public class AreaCalculatorV1 extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void createAndShowGUI() {
		AreaCalculatorV1 window = new AreaCalculatorV1("Area Calculator V1");
		window.createComponents();
		window.setFrameFeature();
	}
	
	protected JPanel panel;
	protected JButton calculatorButton;
	protected JButton resetButton;
	protected JPanel buttonPanel;
	protected JPanel windowPanel;
	
	protected JLabel corner;
	protected JLabel width;
	protected JLabel length;
	protected JLabel area;
	
	protected JRadioButton TriangleButton;
	protected JRadioButton RectangleButton;

	protected JTextField widthTxtField;
	protected JTextField areaTxtField;
	protected JTextField lengthTxtField;
	protected JPanel panelForm;
	protected JPanel osPanel;
	
	protected JLabel features;
	protected JList<String> featuresList;
	protected JMenuBar menuBar;
	protected JMenu menuEdit;
	protected JMenuItem increase;
	protected JMenuItem decrease;
	protected JMenuItem quit;
	
	public AreaCalculatorV1(String title) {
		super(title);
	}

	protected void createComponents() {
		
		corner = new JLabel(" ");
		length = new JLabel("Length: ");
		lengthTxtField = new JTextField(10);
		
		width = new JLabel("Width: ");
		widthTxtField = new JTextField(10);
		
		area = new JLabel("Area: ");
		areaTxtField = new JTextField(10);
		
		
		buttonPanel = new JPanel();
		calculatorButton = new JButton("Calculator");
		resetButton = new JButton("Reset");
		buttonPanel.add(calculatorButton);
		buttonPanel.add(resetButton);
		windowPanel = (JPanel) this.getContentPane();
		windowPanel.setLayout(new BorderLayout());
		windowPanel.add(buttonPanel, BorderLayout.SOUTH);
		
		ButtonGroup group = new ButtonGroup();
		JRadioButton TriangleButton = new JRadioButton("Triangle");
		JRadioButton RectangleButton = new JRadioButton("Rectangle");
		group.add(TriangleButton);
		group.add(RectangleButton);
		osPanel = new JPanel();
		osPanel.add(TriangleButton);
		osPanel.add(RectangleButton);


		panelForm = new JPanel(new GridBagLayout());
		GridBagConstraints gridBag = new GridBagConstraints();
		gridBag.insets = new Insets(20, 2, 20, 2);
		gridBag.gridx = -1;
		gridBag.gridy = 0;
		gridBag.anchor = GridBagConstraints.LINE_START;
		panelForm.add(corner, gridBag);
		gridBag.gridy ++;
		panelForm.add(length, gridBag);
		gridBag.gridy ++;
		panelForm.add(width, gridBag);
		gridBag.gridy ++;
		panelForm.add(area, gridBag);

		
		gridBag.gridx = 1;
		gridBag.gridy = 0;
		gridBag.anchor = GridBagConstraints.LINE_END;
		panelForm.add(osPanel, gridBag);
		windowPanel.add(panelForm);
		gridBag.gridy ++;
		panelForm.add(lengthTxtField, gridBag);
		gridBag.gridy ++;
		panelForm.add(widthTxtField, gridBag);
		gridBag.gridy ++;
		panelForm.add(areaTxtField, gridBag);
		
		
	}

	protected void setFrameFeature() {
		this.setLocationRelativeTo(null);
		pack();
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(300,400);
		menuBar = new JMenuBar();
		menuEdit = new JMenu("Edit");
		
		increase = new JMenuItem("Increase");
		decrease = new JMenuItem("Decrease");
		quit = new JMenuItem("Quit");
		increase.setIcon(new ImageIcon(("Q2/increase.png")));

		menuBar.add(menuEdit);
		menuEdit.add(increase);
		menuEdit.add(decrease);
		menuEdit.add(quit);
		windowPanel.add(menuBar, BorderLayout.NORTH);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				AreaCalculatorV1 rac = new AreaCalculatorV1("Area Calculator V1");
				rac.createAndShowGUI();
			}
		});
	}

}
