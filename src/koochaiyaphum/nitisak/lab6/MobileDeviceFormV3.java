package koochaiyaphum.nitisak.lab6;
/*
 * Author:Nitisak Koochaiyaphum
 * Id: 613040502-9	
 * Sec: 1
 * Date: March 29,2019
 * 
 *
 */
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV3 extends MobileDeviceFormV2{

	protected static final long serialVersionUID = -6841166972387057031L;
	protected JLabel features;
	protected JList<String> featuresList;
	protected JMenuBar menuBar;
	protected JMenu menuFile;
	protected JMenu menuConfig;
	protected JMenuItem newItem;
	protected JMenuItem openItem;
	protected JMenuItem saveItem;
	protected JMenuItem exitItem;
	protected JMenuItem colorItem;
	protected JMenuItem sizeItem;
	
	public MobileDeviceFormV3(String title) {
		super(title);
	}
	protected void addComponents() {
		super.addComponents();
		JLabel features = new JLabel("Features : ");
		String featureData[] = { "Design and build quality", "Great Camera", "Screen", "Battery Life"};
		JList<String> featuresList = new JList<String>(featureData);
		featuresList.setPreferredSize(new Dimension(165, 73));
		GridBagConstraints gridBag = new GridBagConstraints();
		gridBag.insets = new Insets(3, 80, 3, 80);
		gridBag.anchor = GridBagConstraints.LINE_START;
		gridBag.gridy = 7;
		panelForm.add(features, gridBag);
		panelForm.add(featuresList, gridBag);
	}
	protected void addMenus() {
		menuBar = new JMenuBar();
		menuFile = new JMenu("File");
		menuConfig = new JMenu("Config");
		newItem = new JMenuItem("New");
		openItem = new JMenuItem("Open");
		saveItem = new JMenuItem("Save");
		exitItem = new JMenuItem("Exit");
		colorItem = new JMenuItem("Color");
		sizeItem = new JMenuItem("Size");
		menuBar.add(menuFile);
		menuBar.add(menuConfig);
		menuFile.add(newItem);
		menuFile.add(openItem);
		menuFile.add(saveItem);
		menuFile.add(exitItem);
		menuConfig.add(colorItem);
		menuConfig.add(sizeItem);
		windowPanel.add(menuBar, BorderLayout.NORTH);
		
	}
	public static void createAndShowGUI() {
		MobileDeviceFormV3 mobileDeviceFormV3 = new MobileDeviceFormV3("Mobile Device Form V3");
		mobileDeviceFormV3.addComponents();
		mobileDeviceFormV3.addMenus();
		mobileDeviceFormV3.setFrameFeature();
	}
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	

}
