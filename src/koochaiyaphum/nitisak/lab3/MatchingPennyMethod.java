/*This Java program that accepts one arguments:simulates a coin tossing game called matching penny. 
* The program reads in a choice (Head or Tail) 
 *Method separate work in class
 * 
 * Author: Nitisak Koochaiyaphum
 * ID: 61304050-9
 * Sec: 1
 * Date: February 2, 2019
 */
package koochaiyaphum.nitisak.lab3;
import java.util.Random;
import java.util.Scanner;
public class MatchingPennyMethod {
	private static final Scanner scanner = new Scanner( System.in );
	static void acceptInput() { 
	 	  while (true) {
	 	   	System.out.print( "Enter head or tail :" );
	        String input = scanner.nextLine();
	        String input2 = input.toLowerCase();
	         
	         if (input2.equalsIgnoreCase("head") || input2.equalsIgnoreCase("tail")) {
	         	System.out.println( "You play " + input2 );
	         	String [] arr = {"head","tail"};
	         }
	         else{ 
	          	System.err.println( "Incorrect input. head or tail"); 
	          	}
	 	 }
	}
	static void genComChoice() {
		String input = scanner.nextLine();
        String input2 = input.toLowerCase();
		 if (input2.equalsIgnoreCase("head") || input2.equalsIgnoreCase("tail")) {
	         	System.out.println( "You play " + input2 );
	         	String [] arr = {"head","tail"};
	         	Random random = new Random();
	         	int select = random.nextInt(arr.length); 
	         	System.out.println("Computer plays " + arr[select]);
		 }
	}
	static void displayWiner() {
		String input = scanner.nextLine();
        String input2 = input.toLowerCase();
        String [] arr = {"head","tail"};
     	Random random = new Random();
     	int select = random.nextInt(arr.length); 
		if (input2.equalsIgnoreCase(arr[select])){
     		System.out.println("You win");
     	}
     	else {
     		System.out.println("Computer win");
     	}	
	}
	
}
