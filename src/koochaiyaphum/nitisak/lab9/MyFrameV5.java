package koochaiyaphum.nitisak.lab9;
/*
 * Author: Nitisak Koochaiyaphum
 *Id: 613040502-9
 *Sec: 1
 *Date: April, 10,2019
 * 
 */
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import koochaiyaphum.nitisak.lab8.MyFrameV4;

public class MyFrameV5 extends MyFrameV4  {
	public MyFrameV5(String text) {
		super(text);
	}

	public static void main (String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run () {
				createAndShowGUI();
			}
		});
	}
	
	public static void createAndShowGUI() {
		MyFrameV5 msw = new MyFrameV5 ("My Frame V5");
		msw.addComponents();
		msw.setFrameFeatures ();
	}
	
	protected void addComponents() {
		add(new MyCanvasV5());
			
	}
	protected void setFrameFeatures() {
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
