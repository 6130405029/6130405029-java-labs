package koochaiyaphum.nitisak.lab8;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class MyFrameV3 extends MyFrameV2 {
	public MyFrameV3(String text) {
		super(text);
	}

	public static void main (String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run () {
				createAndShowGUI();
			}
		});
	}
	
	public static void createAndShowGUI() {
		MyFrameV3 msw = new MyFrameV3 ("My Frame V3");
		msw.addComponents();
		msw.setFrameFeatures ();
	}
	
	protected void addComponents() {
		add(new MyCanvasV3());
			
	}
	protected void setFrameFeatures() {
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
