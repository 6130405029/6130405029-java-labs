/*This Java program that accepts five arguments:
* 
* Author: Nitisak Koochaiyaphum
* ID: 61304050-9
* Sec: 1
* Date: February 5, 2019
* 
*/
package koochaiyaphum.nitisak.lab3;

import java.util.Arrays;

public class BasicStat {
	public static  {
		static double num[];
		static int number;

		static void acceptInput(String[] args) {
			number = Integer.valueOf(args[0]);
			if (number  == 5) {
				for (int i = 0; i < args.length; i++) {
					num[i] = Integer.valueOf(args[i + 1]);

				}

			} else {
				System.err.print("<BasicStat> <number> <number>...");

			}
		}
		static void displayStats() {
			Arrays.sort(num);
			double minNumber = num[0];
			double maxNumber = num[-1];
		   System.out.println("Max is"+ maxNumber+"min is"+ minNumber);
		}
	}
}
