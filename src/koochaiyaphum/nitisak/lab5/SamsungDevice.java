package koochaiyaphum.nitisak.lab5;

public class SamsungDevice extends MobileDevice{
	private double androidVersion;
	private static String brand = "Samsung";
	private int price;

	public SamsungDevice(String modelName, int price, int weigth,double androidVersion ) {
		super(modelName,"Andriod", price, weigth);
		this.androidVersion =  androidVersion;

	}	
	public String toString() {
		return "SamsungDevice [Model name : " + this.getModelName() + ", os : " + this.getOs() + ", Price : " + this.getPrice() + " Bath " + ", Weight : " + this.getWeigth() + " g, " + " Android Version :" + androidVersion + "]";
		}
	public static String getBrand() {
		return brand;
	}

	public double getAndroidVersion() {
		return androidVersion;
	}
	public void setAndroidVersion(double androidVersion) {
		this.androidVersion = androidVersion;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}

}
