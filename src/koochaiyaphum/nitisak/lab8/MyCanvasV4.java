package koochaiyaphum.nitisak.lab8;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class MyCanvasV4  extends  MyCanvasV3{
	public void paint(Graphics g) {
		super.paint(g);
		
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.black);
		g2d.fillRect(0, 0, this.getWidth(), this.getHeight());
		
		int width = 80;
		int height = 20;
		
		
		Color[] color = {Color.red, Color.orange, Color.yellow, Color.green,  Color.CYAN, Color.blue, Color.MAGENTA};
		g2d.setStroke(new BasicStroke(5));
		for(int i = 0; i < 10; i++) {
			for(int j = 0; j < 7; j++) {
				brick.x = 0 + i * width;
				brick.y = 50 + j * height;
				g2d.setColor(color[j]);
				g2d.fill(brick);
				g2d.setColor(Color.black);
				g2d.draw(brick);
				
			}
			MyBrick brick = new MyBrick(0, 0);
			MyBall ball = new MyBall(0, 0);
			ball.x = this.getWidth()/2 -5;
			ball.y = this.getHeight() - 40;
			brick.x = this.getWidth()/2 - 30;
			brick.y = this.getHeight() - 10;
			g2d.setColor(Color.WHITE);
			g2d.fill(ball);
			g2d.setColor(Color.GRAY);
			g2d.fill(brick);
			
		}
		

	}
}
