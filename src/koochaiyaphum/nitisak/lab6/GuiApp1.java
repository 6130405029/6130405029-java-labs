package koochaiyaphum.nitisak.lab6;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
public class GuiApp1 {
	public static void main(String[] args) {
		new GuiApp1();
		}
	public  GuiApp1(){
		JFrame guiFrame = new JFrame();
		
		guiFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		guiFrame.setTitle("MySimpleWindow");
		guiFrame.setSize(300,100);
		
		guiFrame.setLocationRelativeTo(null);

		
		final JPanel listPanel = new JPanel();
		JButton CancelBut = new JButton( "Cancel");
		JButton OKBut = new JButton( "OK");
		
		CancelBut.addActionListener(new ActionListener()
		{
		@Override
		public void actionPerformed(ActionEvent event)
		{
		
		listPanel.setVisible(!listPanel.isVisible());
		}
		});
	
		guiFrame.add(CancelBut, BorderLayout.NORTH);
		guiFrame.add(OKBut,BorderLayout.SOUTH);
		guiFrame.setVisible(true);
		}

}
