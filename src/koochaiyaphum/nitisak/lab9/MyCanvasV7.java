package koochaiyaphum.nitisak.lab9;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Rectangle2D.Double;

import koochaiyaphum.nitisak.lab8.MyBall;
import koochaiyaphum.nitisak.lab8.MyBrick;
import koochaiyaphum.nitisak.lab8.MyCanvas;
/*
 * Author: Nitisak Koochaiyaphum
 *Id: 613040502-9
 *Sec: 1
 *Date: April, 10,2019
 * 
 */

public class MyCanvasV7 extends MyCanvasV6 implements Runnable {

	protected int numBricks = MyCanvas.WIDTH / MyBrick.brickWidth;
	protected MyBrickV2[] bricks;
	protected Thread running;
	// protected MyBallV2 ball;

	public MyCanvasV7() {
		
		ball = new MyBallV2(0, 0);
		ball.ballVelX = 2;
		ball.ballVelY = 2;
		running = new Thread(this);
		
		bricks = new MyBrickV2[numBricks];
		for (int i = 0; i < numBricks; i++) {
			bricks[i] = new MyBrickV2(MyBrick.brickWidth  * i, MyCanvasV7.HEIGHT / 2);
			
		}

		running.start();
	}

	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.black);
		g2d.fill(new Rectangle2D.Double(0, 0, this.getWidth(), this.getHeight()));
		g2d.setColor(Color.white);
		g2d.fill(ball);

		for (int i = 0; i < numBricks; i++) {
			if(bricks[i].visible == false) {
				

			}else {
				g2d.setColor(Color.RED);
				g2d.fill(bricks[i]);
				g2d.setStroke(new BasicStroke(5));
				g2d.setColor(Color.BLUE);
				g2d.draw(bricks[i]);
			}

		}

	}
	
	public void checkCollition(MyBallV2 ball, MyBrickV2 brick) {
		double x = ball.x + MyBall.diameter / 2.0;
		double y = ball.y + MyBall.diameter / 2.0;
		double deltaX = x - Math.max(brick.x, Math.min(x, brick.x + MyBrick.brickWidth));
		double deltaY = y - Math.max(brick.y, Math.min(y, brick.y + MyBrick.brickWidth));

		boolean collided = (deltaX * deltaX + deltaY * deltaY) < (MyBall.diameter * MyBall.diameter) / 4.0;

		if (collided) {
			if (deltaX * deltaX < deltaY * deltaY) {
				ball.ballVelY *= -1;
			} else {
				ball.ballVelX *= -1;
			}
			ball.move();
			brick.visible = false;
		}
	}
	@Override
	public void run() {
		while (true) {

			if (ball.x <= 0 || ball.x + MyBall.diameter >= MyCanvas.WIDTH - 30) {
				ball.ballVelX *= -1;
			}

			if (ball.y <= 0 || ball.y + MyBall.diameter >= MyCanvas.HEIGHT - 30) {
				ball.ballVelY *= -1;

			}

			for (int i = 0; i < numBricks; i++) {
				if (bricks[i].visible) {
					checkCollition(ball, bricks[i]);

				}
			}

			ball.move();
			repaint();

			try {

				Thread.sleep(12);
			} catch (InterruptedException ex) {

			}
		}

	}

}
