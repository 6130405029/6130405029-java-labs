package koochaiyaphum.nitisak.lab8;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;

public class MyCanvasV2 extends MyCanvas {
	protected  MyBall Ball;
	protected  MyBrick Brick;
	protected  MyPedal Pedal;
							//385,285
	MyBall ball = new MyBall(385,285);
	MyPedal pedal = new MyPedal(351,590);
	MyBrick brick = new MyBrick(360,0);
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		Graphics2D g2d = (Graphics2D) g;
		g.setColor(Color.BLACK);
		g.drawRect(0,0, WIDTH, HEIGHT);
		
		g2d.fillRect(0, 0, WIDTH, HEIGHT);
		g2d.setColor(Color.white);
		g2d.fill(ball);
		g2d.fill(brick);
		g2d.fill(pedal);
		g.drawLine(0, 300, 800, 300);
		g.drawLine(400, 0, 400, 600);
	}
}
