package koochaiyaphum.nitisak.lab9;
/*
 * Author: Nitisak Koochaiyaphum
 *Id: 613040502-9
 *Sec: 1
 *Date: April, 10,2019
 * 
 */
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class MyFrameV8 extends MyFrameV7 {
	public MyFrameV8(String text) {
		super(text);
	}

	public static void main (String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run () {
				createAndShowGUI();
			}
		});
	}
	
	public static void createAndShowGUI() {
		MyFrameV8 msw = new MyFrameV8 ("My Frame V8");
		msw.addComponents();
		msw.setFrameFeatures ();
	}
	
	protected void addComponents() {
		add(new MyCanvasV8());
			
	}
	protected void setFrameFeatures() {
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
