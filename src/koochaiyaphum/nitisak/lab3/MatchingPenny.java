/*
This Java program that accepts one arguments:simulates a coin tossing game called matching penny. 
* The program reads in a choice (Head or Tail) 
* from a user then compares that choice with a randomly generated choice from a computer. 
* If the choices are the same, the user wins. 
* If the choices are different, the computer wins.
* 
* Author: Nitisak Koochaiyaphum
* ID: 61304050-9
* Sec: 1
* Date: February 2, 2019
**/

package koochaiyaphum.nitisak.lab3;



import java.util.Random;
import java.util.Scanner;
public class MatchingPenny {
	private static final Scanner scanner = new Scanner( System.in );
    public static void main ( String [] args ){
 	   
 	  while (true) {
 	   	System.out.print( "Enter head or tail :" );
        String input = scanner.nextLine();
        String input2 = input.toLowerCase();
         
         if (input2.equalsIgnoreCase("head") || input2.equalsIgnoreCase("tail")) {
         	System.out.println( "You play " + input2 );
         	String [] arr = {"head","tail"};
         	Random random = new Random();
         	int select = random.nextInt(arr.length); 
         	System.out.println("Computer plays " + arr[select]); 
         	
         	if (input2.equalsIgnoreCase(arr[select])){
         		System.out.println("You win");
         	}
         	else {
         		System.out.println("Computer win");
         	}
             }
         if (input2.equalsIgnoreCase("exit")){
         	System.out.println("Good bye");
         	System.exit(0);
         	}
         if (input2.equals("head") || input2.equals("tail")) {	
         }
         		
         else{ 
         	System.err.println( "Incorrect input. head or tail"); 
         	}
      }
  }

}
