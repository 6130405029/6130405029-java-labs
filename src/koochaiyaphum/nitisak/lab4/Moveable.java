package koochaiyaphum.nitisak.lab4;

public interface Moveable {
	void accelerate() ;
	void brake();
	void setSpeed();
}
