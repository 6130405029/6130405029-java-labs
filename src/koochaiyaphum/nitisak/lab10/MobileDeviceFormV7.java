package koochaiyaphum.nitisak.lab10;
/*
 * Author:Nitisak Koochaiyaphum
 * Id: 613040502-9	
 * Sec: 1
 * Date: May 1,2019
 */

import javax.swing.SwingUtilities;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import koochaiyaphum.nitisak.lab7.MobileDeviceV6;

public class MobileDeviceFormV7 extends MobileDeviceV6{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MobileDeviceFormV7(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		// Mobile Device Form V7
		MobileDeviceFormV7 mobileDeviceFormV7 = new MobileDeviceFormV7("Mobile Device Form V7");
		mobileDeviceFormV7.addComponents();
		mobileDeviceFormV7.addMenu();
		mobileDeviceFormV7.initComponents();
		mobileDeviceFormV7.setFrameFeatures();
		mobileDeviceFormV7.addListeners();
	}

	@Override
	protected void addComponents() {
		super.addComponents();
		list.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		list.setSelectedIndices(new int[] { 0, 1, 3 });
	}

	@Override
	public void itemStateChanged(ItemEvent event) {
		// TODO Auto-generated method stub
		Object src = event.getSource();
		if (src == androidRadio || src == osRadio) {
			if (androidRadio.isSelected()) {
				JOptionPane.showMessageDialog(this, "Your os platform is now changed to Android");

			} else if (osRadio.isSelected()) {
				JOptionPane.showMessageDialog(this, "Your os platform is now changed to iOS");

			}

		} else if (event.getStateChange() == ItemEvent.SELECTED) {
			JOptionPane.showMessageDialog(this, "Type is updated to " + typeComboBox.getSelectedItem());
		}

	}

	@Override
	public void actionPerformed(ActionEvent event) {
		// TODO Auto-generated method stub
		Object src = event.getSource();
		if (src == okButton) {
			handleOKButton();

		} else if (src == cancelButton) {
			handleCancelButton();
		}
	}

	private void handleCancelButton() {
		// TODO Auto-generated method stub
		textArea.setText("");
		nameTextField.setText("");
		modelTextField.setText("");
		weightTextField.setText("");
		priceTextField.setText("");
	}

	private void handleOKButton() {
		// TODO Auto-generated method stub
		if (androidRadio.isSelected()) {
			JOptionPane.showMessageDialog(this, "Brand Name: " + nameTextField.getText() + ", Model Name: "
					+ modelTextField.getText() + ", Weight: " + weightTextField.getText() + ", Price: "
					+ priceTextField.getText() + "\nOS: " + androidRadio.getText() + "\nType: "
					+ typeComboBox.getSelectedItem() + "\nFeatures: " + list.getSelectedValuesList().toString()
							.substring(1, list.getSelectedValuesList().toString().length() - 1)
					+ "\nReview: " + textArea.getText());
		} else {
			JOptionPane.showMessageDialog(this, "Brand Name: " + nameTextField.getText() + ", Model Name: "
					+ modelTextField.getText() + ", Weight: " + weightTextField.getText() + ", Price: "
					+ priceTextField.getText() + "\nOS: " + osRadio.getText() + "\nType: "
					+ typeComboBox.getSelectedItem() + "\nFeatures: " + list.getSelectedValuesList().toString()
							.substring(1, list.getSelectedValuesList().toString().length() - 1)
					+ "\nReview: " + textArea.getText());
		}

	}

	protected void addListeners() {
		okButton.addActionListener(this);
		cancelButton.addActionListener(this);
		androidRadio.addItemListener(this);
		osRadio.addItemListener(this);
		typeComboBox.addItemListener(this);
		list.addListSelectionListener(this);
	}

	@Override
	public void valueChanged(ListSelectionEvent arg0) {
		// TODO Auto-generated method stub
		String features = "";
		Object obj[] = list.getSelectedValues();
		for (int i = 0; i < obj.length; i++) {
			if (obj.length == 1) {
				features += (String) obj[i];
			} else {
				if (i == obj.length - 1) {
					features += (String) obj[i];
				} else {
					features += (String) obj[i] + ", ";
				}
			}

		}
		JOptionPane.showMessageDialog(this, features);

	}
}
