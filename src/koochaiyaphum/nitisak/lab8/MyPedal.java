package koochaiyaphum.nitisak.lab8;

import java.awt.Dimension;
import java.awt.geom.Rectangle2D;

public class MyPedal extends  Rectangle2D.Double{
	public final static int  pedalWidth  = 100;
	public final static int  pedalHeight  = 10;
	
	public MyPedal(int x, int y) {
		super(x, y, pedalWidth, pedalHeight);
	}
}
