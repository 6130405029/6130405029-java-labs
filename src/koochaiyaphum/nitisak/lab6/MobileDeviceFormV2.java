package koochaiyaphum.nitisak.lab6;

/*
 * 
 * 
 * 
 * 
 * 
 */
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
/*
 * Author:Nitisak Koochaiyaphum
 * Id: 613040502-9	
 * Sec: 1
 * Date: March 29,2019
 * 
 */
public class MobileDeviceFormV2  extends MobileDeviceFormV1{
	protected static final long serialVersionUID = 6122763260256216642L;
	protected JLabel typePhone;
	protected JComboBox<String> comboType;
	protected JTextField typeTxtField;
	protected JLabel review;
	protected JPanel reviewPanel;
	public MobileDeviceFormV2(String title) {
		super(title);
	}

	protected void addComponents() {
		super.addComponents();
		typePhone = new JLabel("Type : ");
		JComboBox<String> comboType = new JComboBox<String>();
		comboType.addItem("Phone");
		comboType.addItem("Tablet");
		comboType.addItem("Smart TV");
		comboType.setEditable(true);
		comboType.setPreferredSize(new Dimension(165,22));
		review = new JLabel("Review : ");
		JTextArea reviewTextArea = new JTextArea(3, 35);
		reviewTextArea.setLineWrap(true);
		reviewTextArea.setWrapStyleWord(true);
		reviewTextArea.setText("Bigger than previous Note phones in every way, "
				+ "the Samsung Galaxy Note 9 has a larger 6.4-inch screen, "
				+ "heftier 4,000mAh battery, and a massive 1TB of storage option.");
		JScrollPane scrollPane = new JScrollPane(reviewTextArea);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		reviewPanel = new JPanel(); 
		reviewPanel.add(scrollPane);
		GridBagConstraints gridBag = new GridBagConstraints();
		gridBag.insets = new Insets(3, 80, 3, 80);
		gridBag.anchor = GridBagConstraints.LINE_START;
		gridBag.gridy = 6;
		panelForm.add(typePhone,gridBag);
		panelForm.add(comboType,gridBag);
		gridBag.gridy = 8; 
		panelForm.add(review, gridBag);
		gridBag.gridy = 9;
		gridBag.gridwidth = 3;
		gridBag.fill = GridBagConstraints.HORIZONTAL;
		panelForm.add(reviewPanel, gridBag);
		
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV2 mobileDeviceFormV2 = new MobileDeviceFormV2("Mobile Device Form V2");
		mobileDeviceFormV2.addComponents();
		mobileDeviceFormV2.setFrameFeature();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
