package koochaiyaphum.nitisak.lab10;
/*
 * Author:Nitisak Koochaiyaphum
 * Id: 613040502-9	
 * Sec: 1
 * Date: May 1,2019
 */

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
public class MobileDeviceFormV8 {
	MobileDeviceFormV8 extends MobileDeviceFormV7 {
		protected JMenuItem customMenu;

		public MobileDeviceFormV8(String title) {
			super(title);
			// TODO Auto-generated constructor stub
		}

		public static void main(String[] args) {
			// TODO Auto-generated method stub
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					createAndShowGUI();
				}
			});
		}
		public static void createAndShowGUI() {
			// Mobile Device Form V8
			MobileDeviceFormV8 mobileDeviceFormV8 = new MobileDeviceFormV8("Mobile Device Form V8");
			mobileDeviceFormV8.addComponents();
			mobileDeviceFormV8.addMenu();
			mobileDeviceFormV8.initComponents();
			mobileDeviceFormV8.setFrameFeatures();
			mobileDeviceFormV8.addListeners();
			mobileDeviceFormV8.addShortcut();
		}
		
		@Override
		protected void addSubMenu() {
			super.addSubMenu();
			customMenu = new JMenuItem("Custom ...");
			colorMenu.add(customMenu);
		}
		// add mnemonic key
		protected void addShortcut() {
			menuFile.setMnemonic(KeyEvent.VK_F);
			configFile.setMnemonic(KeyEvent.VK_C);
			newItem.setMnemonic(KeyEvent.VK_N);
			newItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N,ActionEvent.ALT_MASK));
			openItem.setMnemonic(KeyEvent.VK_O);
			openItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O,ActionEvent.ALT_MASK));
			saveItem.setMnemonic(KeyEvent.VK_S);
			saveItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,ActionEvent.ALT_MASK));
			exitItem.setMnemonic(KeyEvent.VK_X);
			exitItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X,ActionEvent.ALT_MASK));
			colorMenu.setMnemonic(KeyEvent.VK_L);
			redColor.setMnemonic(KeyEvent.VK_R);
			redColor.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R,ActionEvent.ALT_MASK));
			greenColor.setMnemonic(KeyEvent.VK_G);
			greenColor.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G,ActionEvent.ALT_MASK));
			blueColor.setMnemonic(KeyEvent.VK_B);
			blueColor.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B,ActionEvent.ALT_MASK));
			customMenu.setMnemonic(KeyEvent.VK_U);
			customMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U,ActionEvent.ALT_MASK));
			
		}
}
