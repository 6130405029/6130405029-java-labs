package koochaiyaphum.nitisak.lab8;

import java.awt.Dimension;
import java.awt.geom.Rectangle2D;

public class MyBrick extends Rectangle2D.Double{
	public final static int  brickWidth = 80;
	public final static int  brickHeight = 20;
	
	public MyBrick(int x, int y ) {
		super(x, y, brickWidth, brickHeight);
	}

}
