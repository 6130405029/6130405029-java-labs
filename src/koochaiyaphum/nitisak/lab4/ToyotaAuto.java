package koochaiyaphum.nitisak.lab4;

//import koochaiyaphum.nitisak.lab4.Automobile.Color;
public class ToyotaAuto extends Automobile implements Moveable, Refuelable  {

	public ToyotaAuto(int maximumspeed, int acceleration, String model) {

		this.setAcceleration(acceleration);
		this.setMaxSpeed(maximumspeed);
		this.setModel(model);
		this.setGasoline(100);
	}
	@Override
	public void refuel() {
		this.setGasoline(100);
		System.out.printf("%s refuels\n", this.getModel());
	}

	@Override
	public void accelerate() {
		if (this.getSpeed() + this.getAcceleration() <= this.getMaxSpeed()) {
			this.setSpeed(this.getSpeed() + this.getAcceleration());
			this.setGasoline(this.getGasoline() - 15);
			}
		else {
			this.setSpeed(this.getMaxSpeed());
			this.setGasoline(this.getGasoline() - 15);
			}
		System.out.printf("%s accelerates\n", this.getModel());
		
	}

	@Override
	public void brake() {
		if (this.getSpeed() - this.getAcceleration() >= 0) {
			this.setSpeed(this.getSpeed() - this.getAcceleration());
			this.setGasoline(this.getGasoline() - 15);
			}
		else {
			this.setSpeed(0);
			this.setGasoline(this.getGasoline() - 15);
			}
		System.out.printf("%s brakes\n", this.getModel());
	}

	@Override
	public void setSpeed() {
		 if(this.getSpeed()<=0||this.getSpeed()>getMaxSpeed()) {
	    	  this.setSpeed(0);
	      }
	}
}
