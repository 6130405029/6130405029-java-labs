package koochaiyaphum.nitisak.lab9;
/*
 * Author: Nitisak Koochaiyaphum
 *Id: 613040502-9
 *Sec: 1
 *Date: April, 10,2019
 * 
 */

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Rectangle2D;
import java.util.Random;

import koochaiyaphum.nitisak.lab8.MyBall;
import koochaiyaphum.nitisak.lab8.MyBrick;
import koochaiyaphum.nitisak.lab8.MyCanvas;
import koochaiyaphum.nitisak.lab8.MyPedal;

public class MyCanvasV8 extends MyCanvasV7 implements Runnable, KeyListener {
	protected int numCol = MyCanvas.WIDTH / MyBrick.brickWidth;
	protected int numRow = 7;
	protected int numVisibleBricks = numCol * numRow;
	protected MyBrickV2[][] bricks;
	protected Thread running;
	protected Color[] color = { Color.MAGENTA, Color.BLUE, Color.CYAN, Color.GREEN, Color.YELLOW, Color.ORANGE,Color.RED };
	protected MyPedalV2 pedal;
	protected int lives;

	public MyCanvasV8() {
		bricks = new MyBrickV2[numRow][numCol];
		ball = new MyBallV2(MyCanvas.WIDTH / 2 , MyCanvas.HEIGHT - MyBall.diameter- 30 - MyPedal.pedalHeight);
		ball.ballVelX = 0;
		ball.ballVelY = 0;
		running = new Thread(this);

		for (int i = 0; i < numRow; i++) {
			for (int j = 0; j < numCol; j++) {
				bricks[i][j] = new MyBrickV2(j * MyBrick.brickWidth, super.HEIGHT / 3 - i * MyBrick.brickHeight);
			}
		}

		running.start();
		setFocusable(true);
		addKeyListener(this);
		pedal = new MyPedalV2(MyCanvas.WIDTH / 2 - MyPedal.pedalWidth / 2, MyCanvas.HEIGHT - MyPedal.pedalHeight);
		lives = 3;
	}

	public int getLives() {
		return lives;
	}

	public void setLives(int lives) {
		this.lives = lives;
	}

	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.BLACK);
		g2d.fill(new Rectangle2D.Double(0, 0, super.WIDTH, super.HEIGHT));

		for (int i = 0; i < numRow; i++) {
			for (int j = 0; j < numCol; j++) {
				if (bricks[i][j].visible) {
					g2d.setColor(color[i]);
					g2d.fill(bricks[i][j]);
					g2d.setStroke(new BasicStroke(4));
					g2d.setColor(Color.BLACK);
					g2d.draw(bricks[i][j]);
				}

			}

			g2d.setColor(Color.WHITE);
			g2d.fill(ball);

			g2d.setColor(Color.GRAY);
			g2d.fill(pedal);

			g2d.setFont(new Font("SanSerif", Font.BOLD, 20));
			g2d.setColor(Color.BLUE);
			String s = " Lives : " + lives;
			g2d.drawString(s, 10, 30);

			

			if (numVisibleBricks == 0) {
				String won = "You won";
				g2d.setFont(new Font("SanSerif", Font.BOLD, 50));
				g2d.setColor(Color.GREEN);
				g2d.drawString(won, super.WIDTH / 2 - 100, super.HEIGHT / 2);
				break;

			}

			if (lives == 0) {
				String over = "GAME OVER";
				g2d.setFont(new Font("SanSerif", Font.BOLD, 50));
				g2d.setColor(Color.GRAY);
				g2d.drawString(over, super.WIDTH / 2 - 150, super.HEIGHT / 2);
				break;

			} else if (lives < 0) {
				System.exit(0);
			}

		}

	}

	public void checkPassBottom() {
		if (ball.y >= MyCanvas.HEIGHT) {
			ball.x = pedal.x + MyPedal.pedalWidth / 2 - MyBall.diameter / 2;
			ball.y = MyCanvas.HEIGHT - MyBall.diameter - MyPedal.pedalHeight;
			ball.ballVelX = 0;
			ball.ballVelY = 0;
			lives--;
			repaint();
		}
	}

	public void checkCollition(MyBallV2 ball, MyBrickV2 brick) {
		double x = ball.x + MyBall.diameter / 2.0;
		double y = ball.y + MyBall.diameter / 2.0;
		double deltaX = x - Math.max(brick.x, Math.min(x, brick.x + MyBrick.brickWidth));
		double deltaY = y - Math.max(brick.y, Math.min(y, brick.y + MyBrick.brickWidth));

		boolean collided = (deltaX * deltaX + deltaY * deltaY) < (MyBall.diameter * MyBall.diameter) / 4.0;

		if (collided) {
			if (deltaX * deltaX < deltaY * deltaY) {
				ball.ballVelY *= -1;
			} else {
				ball.ballVelX *= -1;
			}
			numVisibleBricks--;
			ball.move();
			brick.visible = false;

		}
	}

	private void collideWithPedal() {
		double x = ball.x + MyBall.diameter / 2.0;
		double y = ball.y + MyBall.diameter / 2.0;
		double deltaX = x - Math.max(pedal.x, Math.min(x, pedal.x + MyPedalV2.pedalWidth));
		double deltaY = y - Math.max(pedal.y, Math.min(y, pedal.y + MyPedalV2.pedalHeight));

		boolean collided = (deltaX * deltaX + deltaY * deltaY) < (MyBall.diameter * MyBall.diameter) / 4.0;

		if (collided) {

			if (deltaX * deltaX < deltaY * deltaY) {
				ball.ballVelY *= -1;
			} else {
				ball.ballVelX *= -1;
			}

			ball.move();
		}

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_LEFT) {
			pedal.moveLeft();
		} else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
			pedal.moveRight();
		} else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
			ball.ballVelX = 4;
			Random ran = new Random();
			int randomNum = ran.nextInt(2);
			if (randomNum == 0) {
				ball.ballVelY = 4;
			} else if (randomNum == 1) {
				ball.ballVelY = -4;
			}
		}

	}

	@Override
	public void run() {
		while (true) {
			if (ball.x + MyBall.diameter >= super.WIDTH) {
				ball.x = super.WIDTH - MyBall.diameter;
				ball.ballVelX *= -1;
			} else if (ball.x + MyBall.diameter <= 0) {
				ball.x = 0;
				ball.ballVelX *= -1;
			} else if (ball.y + MyBall.diameter <= 0) {
				ball.y = 0;
				ball.ballVelY *= -1;
			}

			for (int i = 0; i < numRow; i++) {
				for (int j = 0; j < numCol; j++) {
					if (bricks[i][j].visible) {
						checkPassBottom();
						collideWithPedal();
						checkCollition(ball, bricks[i][j]);

					}
				}
			}

			ball.move();
			repaint();

			try {
				Thread.sleep(1);
			} catch (InterruptedException ex) {

			}
		}
	}

	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}
}
