/**This Java program that accepts three arguments:you will input the number of notes of
* 1,000 Baht, 500 Baht, 100 Baht and 20 Baht.
* amount of money you have.
* the program is in the format
* "Total money is " + <total money>
* 
* Author: Nitisak Koochaiyaphum
* ID: 61304050-9
* Sec: 1
* Date: January 26, 2019
**/
package koochaiyaphum.nitisak.lab2;
public class ComputeMoney {
	public static void main(String[] args) {
		if (args.length != 4) {
			System.err.println("Computer money <1000 Bath> <500 Bath> <100 Bath> <20 Bath>");
		}	
		else 
		{		
			int x = Integer.parseInt(args[0]);
			int y = Integer.parseInt(args[1]);
			int z = Integer.parseInt(args[2]);
			int w = Integer.parseInt(args[3]);
			double price1000 = 1000 * x ;
			double price500 = 500 * y;
			double price100 = 100 * z;
			double price20 = 20 * w ;
			double total = price1000 + price500 +  price100 + price20;
			System.out.println("Total money is " + total);
	}
}
}

