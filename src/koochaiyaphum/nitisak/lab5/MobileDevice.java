package koochaiyaphum.nitisak.lab5;

public class MobileDevice {

	private String modelName;
	private String os;
	private int price;
	private int weigth;
	
	public MobileDevice(String modelName, String os, int price, int weigth) {
		this.modelName =  modelName;
		this.os =  os;
		this.price =  price;
		this.weigth =  weigth;
	}
	
	public MobileDevice(String modelName, String os, int price) {
		this.modelName =  modelName;
		this.os =  os;
		this.price =  price;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getWeigth() {
		return weigth;
	}

	public void setWeigth(int weigth) {
		this.weigth = weigth;
	}

	public String toString() {
		return "MobileDevice [modelName:" + modelName + ", os: " + os + ", price:" + price + ", weigth:" + weigth + " g"+ "]";
	}
	
}
